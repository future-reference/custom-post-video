<?php

/**
 * @wordpress-plugin
 * Plugin Name:       Custom Post Type - Video
 * Plugin URI:        https://gitlab.com/future-reference/custom-post-video
 * Description:       Wordpress Plugin to create a custom post type named video
 * Version:           1.0.0
 * Author:            Future Reference
 * Author URI:        http://futurereference.co/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       custom-post-video
 */

add_action('init', 'video_register_post_types');

function video_register_post_types()
{
    /* Register the Video post type. */
    register_post_type(
        'video',
        array(
            'description'         => '',
            'public'              => true,
            'publicly_queryable'  => true,
            'show_in_nav_menus'   => false,
            'show_in_admin_bar'   => true,
            'exclude_from_search' => false,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'menu_position'       => 20,
            'menu_icon'           => 'dashicons-format-video',
            'can_export'          => true,
            'delete_with_user'    => false,
            'hierarchical'        => false,
            'has_archive'         => true,
            'capability_type'     => 'post',

            /* The rewrite handles the URL structure. */
            'rewrite' => array(
                'slug'       => 'video',
                'with_front' => false,
                'pages'      => true,
                'feeds'      => true,
                'ep_mask'    => EP_PERMALINK,
            ),

            /* What features the post type supports. */
            'supports' => array(
                'title',
                'editor',
                'revisions',
                'thumbnail'
            ),

            /* Labels used when displaying the posts. */
            'labels' => array(
                'name'               => __('Videos',                    'futurereference'),
                'singular_name'      => __('Video',                     'futurereference'),
                'menu_name'          => __('Videos',                    'futurereference'),
                'name_admin_bar'     => __('Videos',                    'futurereference'),
                'add_new'            => __('Add New',                   'futurereference'),
                'add_new_item'       => __('Add New Video',             'futurereference'),
                'edit_item'          => __('Edit Video',                'futurereference'),
                'new_item'           => __('New Video',                 'futurereference'),
                'view_item'          => __('View Video',                'futurereference'),
                'search_items'       => __('Search Videos',             'futurereference'),
                'not_found'          => __('No videos found',           'futurereference'),
                'not_found_in_trash' => __('No videos found in trash',  'futurereference'),
                'all_items'          => __('Videos',                    'futurereference'),
            )
        )
    );
}

add_action('init', 'video_register_taxonomies');
function video_register_taxonomies()
{

    /* Register the Video Type taxonomy. */
    register_taxonomy(
        'video_category',
        array('video'),
        array(
            'public'            => true,
            'show_ui'           => true,
            'show_in_menu'      => true,
            'show_in_nav_menus' => true,
            'show_tagcloud'     => false,
            'show_admin_column' => true,
            'hierarchical'      => true,
            /* The rewrite handles the URL structure. */
            'rewrite' => array(
                'slug'         => 'videos',
                'with_front'   => false,
                'hierarchical' => true,
                'ep_mask'      => EP_NONE
            ),
            /* Labels used when displaying taxonomy and terms. */
            'labels' => array(
                'name'                       => __('Video Categories',          'futurereference'),
                'singular_name'              => __('Video Category',           'futurereference'),
                'menu_name'                  => __('Video Categories',          'futurereference'),
                'name_admin_bar'             => __('Video Category',           'futurereference'),
                'search_items'               => __('Search Video Categories',   'futurereference'),
                'popular_items'              => __('Popular Video Categories',  'futurereference'),
                'all_items'                  => __('All Video Categories',      'futurereference'),
                'edit_item'                  => __('Edit Video Category',      'futurereference'),
                'view_item'                  => __('View Video Category',      'futurereference'),
                'update_item'                => __('Update Video Category',    'futurereference'),
                'add_new_item'               => __('Add New Video Category',   'futurereference'),
                'new_item_name'              => __('New Video Category',       'futurereference'),
                'parent_item_colon'          => __('',                          'futurereference'),
                'separate_items_with_commas' => null,
                'add_or_remove_items'        => null,
                'choose_from_most_used'      => null,
                'not_found'                  => null,
            )
        )
    );
}
